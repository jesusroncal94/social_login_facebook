import facebook
from flask import Flask, jsonify, redirect, render_template, request, session, url_for
from flask_cors import CORS


app = Flask(__name__)
cors = CORS(app, resources={r"/api/*": {"origins": "*"}})


@app.route("/api", methods=['POST'])
def api():
    try:
        values = request.form
        social_token = values.get('social_token', None)

        if social_token:
            graph = facebook.GraphAPI(access_token=social_token)
            profile = graph.request('/me?fields=email,first_name,last_name')
            social_data = {
                'id': profile.get('id', ''),
                'email': profile.get('email', ''),
                'firstName': profile.get('first_name', ''),
                'lastName': profile.get('last_name', '')
            }
            # Registro en BD
            return jsonify({'social_data': social_data}), 200
        else:
            return jsonify({'error': 'Token inválido'}), 404
    except Exception as e:
        return jsonify({'error': 'Token inválido'}), 404


if __name__ == "__main__":
    app.run(
        host='0.0.0.0',
        port=2000,
        debug=True
    )
